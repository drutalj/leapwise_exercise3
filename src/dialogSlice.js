import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const dialogSlice = createSlice({
    name: 'dialog',
    initialState: {
        data: null,
        status: 'idle',
        error: null,
        responseStatus: null,
        responseStatusText: null
    },
    reducers: {
        closeDialog: state => {
            state.status = 'idle';
            state.data = null;
        }
    },
    extraReducers(builder) {
        builder
            .addCase(fetchDialogData.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchDialogData.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = action.payload.data;
                state.responseStatus = action.payload.responseStatus;
                state.responseStatusText = action.payload.responseStatusText;
            })
            .addCase(fetchDialogData.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
    }
});

export const { closeDialog } = dialogSlice.actions

export default dialogSlice.reducer;

export const fetchDialogData = createAsyncThunk('dialog/fetchDialogData', async (id) => {
    const response = await fetch('https://604868d1b801a40017ccdac6.mockapi.io/api/v1/subscriber/' + id);
    let data = null;
    if (response.status === 200) {
        data = await response.json();
    }
    return {
        data: data,
        responseStatus: response.status,
        responseStatusText: response.statusText
    };
});