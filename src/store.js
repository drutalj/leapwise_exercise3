import { configureStore } from '@reduxjs/toolkit'
import pageReducer from './pageSlice';
import dialogReducer from './dialogSlice';

export default configureStore({
    reducer: {
        page: pageReducer,
        dialog: dialogReducer
    }
})