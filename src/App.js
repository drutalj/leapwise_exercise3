import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Container from '@mui/material/Container';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSearchParams } from 'react-router-dom';
import './App.css';
import { closeDialog, fetchDialogData } from './dialogSlice';
import { fetchPage } from './pageSlice';

function App() {
  const pageData = useSelector(state => state.page);
  const dialogData = useSelector(state => state.dialog);
  const dispatch = useDispatch();
  const [searchParams, setSearchParams] = useSearchParams();

  useEffect(() => {
    let page = parseInt(searchParams.get('page'));
    if (isNaN(page)) {
      page = 1;
    }
    dispatch(fetchPage({
      page: page,
    }));
  }, [dispatch, searchParams]);

  let dialogTitle = <></>;
  if (dialogData.status !== 'idle') {
    if (dialogData.status === 'loading') {
      dialogTitle = <div className={'loader'} />;
    } else if (dialogData.status === 'failed') {
      dialogTitle = <Typography sx={{
        color: 'red',
        fontWight: 'bold'
      }}>Error: {dialogData.error}</Typography>;
    } else if (dialogData.responseStatus !== 200) {
      dialogTitle = <Typography sx={{
        color: 'red',
        fontWight: 'bold'
      }}>Error: {dialogData.responseStatus} {dialogData.responseStatusText}</Typography>;
    } else {
      dialogTitle = <div style={{
        display: 'flex',
        alignItems: 'center'
      }}><Avatar
          sx={{ mr: 2 }}
          src={dialogData.data.avatar}
          alt={dialogData.data.name} />
        <Typography sx={{
          fontSize: '1.2rem',
          fontWeight: 'bold'
        }}>{dialogData.data.name}</Typography>
      </div>;
    }
  }

  let ret = <></>;
  if (pageData.status === 'idle' || pageData.status === 'loading') {
    ret = <Box sx={{
      ml: 'auto',
      mr: 'auto',
      mt: 4,
      width: '64px'
    }}>
      <div className={'loader'} />
    </Box>;
  } else if (pageData.status === 'failed') {
    ret = <Typography align={'center'} sx={{
      mt: 4,
      fontSize: '2rem',
      color: 'red',
      fontWight: 'bold'
    }}>Error: {pageData.error}</Typography>;
  } else if (pageData.status === 'succeeded') {
    if (pageData.responseStatus !== 200) {
      ret = <Typography align={'center'} sx={{
        mt: 4,
        fontSize: '2rem',
        color: 'red',
        fontWight: 'bold'
      }}>Error: {pageData.responseStatus} {pageData.responseStatusText}</Typography>;
    } else {
      ret = <>
        <Container maxWidth={'xl'} sx={{
          mt: 2,
          mb: 2
        }}>
          <Grid container spacing={2}>
            {pageData.pageData.map((item, index) => (
              <Grid key={index} item xs={12} sm={6} md={4}>
                <Card>
                  <CardHeader
                    avatar={
                      <Avatar src={item.avatar} alt={item.name} />
                    }
                    title={
                      <Typography sx={{
                        fontSize: '1.2rem',
                        fontWeight: 'bold'
                      }}>{item.name}</Typography>
                    }
                  />
                  <CardContent>
                    <div>ID: {item.id}</div>
                    <div>{item.address}</div>
                    <div>{item.city}</div>
                    <div>{item.country}</div>
                  </CardContent>
                  <CardActions>
                    <Button
                      size={'small'}
                      onClick={() => {
                        dispatch(fetchDialogData(item.id))
                      }}
                    >More</Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
          {(pageData.havePrev || pageData.haveNext) && <Grid container spacing={1} sx={{
            mt: 1
          }}>
            {pageData.havePrev &&
              <Grid item xs={12} sm={'auto'}>
                <Button
                  variant={'contained'}
                  fullWidth={true}
                  onClick={() => {
                    setSearchParams(pageData.currentPage === 2 ? {} : { page: pageData.currentPage - 1 });
                  }}
                >Previous page</Button>
              </Grid>
            }
            {pageData.haveNext &&
              <Grid item xs={12} sm={'auto'}>
                <Button
                  variant={'contained'}
                  fullWidth={true}
                  onClick={() => {
                    setSearchParams({ page: pageData.currentPage + 1 });
                  }}
                >Next page</Button>
              </Grid>
            }
          </Grid>}
        </Container>
        {dialogData.status !== 'idle' && <Dialog
          onClose={() => {
            dispatch(closeDialog());
          }}
          open={true}
        >
          <DialogTitle>{dialogTitle}</DialogTitle>
          {dialogData.status === 'succeeded' && dialogData.responseStatus === 200 &&
            <DialogContent>
              <DialogContentText>
                Calls
              </DialogContentText>
              {dialogData.data.calls.length > 0 ?
                <List>
                  {dialogData.data.calls.map((item, index) => (
                    <ListItem key={'calls-' + index}>
                      <ListItemText
                        primary={item.name}
                        secondary={<>
                          ID: {item.id}<br />
                          Created: {new Date(item.created).toLocaleString()}<br />
                          Balance: {item.balance}<br />
                        </>} />
                    </ListItem>
                  ))}
                </List>
                :
                'No calls'
              }
              <DialogContentText sx={{
                mt: 1
              }}>
                Accounts
              </DialogContentText>
              {dialogData.data.accounts.length > 0 ?
                <List>
                  {dialogData.data.accounts.map((item, index) => (
                    <ListItem key={'accounts-' + index}>
                      <ListItemText
                        primary={item.name}
                        secondary={<>
                          ID: {item.id}<br />
                          Created: {new Date(item.created).toLocaleString()}<br />
                          Balance: {item.balance}<br />
                        </>} />
                    </ListItem>
                  ))}
                </List>
                :
                'No accounts'
              }
            </DialogContent>
          }
        </Dialog>
        }
      </>
    }
  }

  return ret;
}

export default App;
