import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const pageSlice = createSlice({
    name: 'page',
    initialState: {
        currentPage: null,
        pageData: [],
        havePrev: false,
        haveNext: false,
        status: 'idle',
        error: null,
        responseStatus: null,
        responseStatusText: null
    },
    reducers: {},
    extraReducers(builder) {
        builder
            .addCase(fetchPage.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchPage.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.currentPage = action.payload.currentPage;
                state.pageData = action.payload.pageData;
                state.havePrev = action.payload.havePrev;
                state.haveNext = action.payload.haveNext;
                state.responseStatus = action.payload.responseStatus;
                state.responseStatusText = action.payload.responseStatusText;
            })
            .addCase(fetchPage.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
    }
});

export default pageSlice.reducer;

export const fetchPage = createAsyncThunk('page/fetchPage', async (data) => {
    let page = 1;
    if (data.page !== undefined || data.page !== null) {
        page = data.page;
    }
    const response = await fetch('https://604868d1b801a40017ccdac6.mockapi.io/api/v1/subscriber?limit=7&page=' + page);
    let pageData = [];
    if (response.status === 200) {
        pageData = await response.json();
    }
    let responseHaveNext = null;
    try {
        responseHaveNext = await fetch('https://604868d1b801a40017ccdac6.mockapi.io/api/v1/subscriber?limit=7&page=' + (page + 1));
    } catch (e) {
    }
    return {
        currentPage: page,
        pageData: pageData,
        havePrev: page > 1,
        haveNext: responseHaveNext !== null && responseHaveNext.status === 200 && (await responseHaveNext.json()).length > 0,
        responseStatus: response.status,
        responseStatusText: response.statusText
    };
});